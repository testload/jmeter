FROM azul/zulu-openjdk-alpine:12

ENV TZ Europe/Moscow
RUN    apk update \
	&& apk upgrade \
	&& apk add ca-certificates \
	&& apk add --update tzdata curl unzip bash wget nano \
	&& apk add python \
	&& rm -rf /var/cache/apk/*

COPY apache-jmeter-5.1.1 /apache-jmeter-5.1.1
ENV JMETER_HOME /apache-jmeter-5.1.1
RUN chmod +x /apache-jmeter-5.1.1/bin/jmeter & chmod +x /apache-jmeter-5.1.1/bin/startjmfromdocker.sh & chmod +x /apache-jmeter-5.1.1/bin/startjmfromdocker_lazy_listener.py
ENV PATH $JMETER_HOME/bin:$PATH
WORKDIR $JMETER_HOME
ENTRYPOINT ["/apache-jmeter-5.1.1/bin/startjmfromdocker.sh"]