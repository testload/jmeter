#!/bin/bash
set -e
freeMem=`awk '{ print int($1/1024/1024) }' /sys/fs/cgroup/memory/memory.limit_in_bytes`
s=$(($freeMem/10*6))
x=$(($freeMem/10*8))
n=$(($freeMem/10*2))
export JVM_ARGS="-Xmn${n}m -Xms${s}m -Xmx${x}m"

if [ -n $jksFileBase ]
then
 jksFile="-Djavax.net.ssl.keyStore=/apache-jmeter-5.1.1/ext_data_certs/$jksFileBase$containerNum"
 jksPass="-Djavax.net.ssl.keyStorePassword=$jksPassword"
 jksType="-Djavax.net.ssl.keyStoreType=PKCS12"
fi
echo "JKS=${jksFile};${jksType}"

if [ "$LazyListener" == "true" ]
then
 echo "DOWNLOADING and PROCESSING script"
 wget $loadScriptURL -O /base_script.jmx --no-check-certificate && chmod +rrr /base_script.jmx &&
 wget $LazyListenerURL -O /listener_script.jmx --no-check-certificate && chmod +rrr /listener_script.jmx &&
 python $JMETER_HOME/bin/startjmfromdocker_lazy_listener.py
 chmod +rrr /script.jmx
else
 echo "DOWNLOADING script"
 wget $loadScriptURL -O /script.jmx --no-check-certificate && chmod +rrr /script.jmx
fi

echo "START Running Jmeter on `date`"
echo "JVM_ARGS=${JVM_ARGS}"

# Run JM with special and additional parameters
jmeter -n -t /script.jmx $loadProfileName $loadRunId $loadIntencity $loadHoldload $loadPacing $loadHostName $loadInfluxHost $loadInfluxPort $loadInfluxLogin $loadInfluxPassword $loadTaskSlot $param0 $param1 $param2 $param3 $param4 $param5 $param6 $param7 $param8 $param9 ${jksFile} ${jksPass} ${jksType}
echo "END Running Jmeter on `date`"